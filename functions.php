<?php

update_option('siteurl','http://blog.safeauto.com');
update_option('home','http://blog.safeauto.com');
/*
 * WordPress Sample function and action
 * for loading scripts in themes
 */
 
add_theme_support( 'post-thumbnails' );
 
// Let's hook in our function with the javascript files with the wp_enqueue_scripts hook 
 
add_action( 'wp_enqueue_scripts', 'safeauto_load_javascript_files' );
 
// Register some javascript files, because we love javascript files. Enqueue a couple as well 
 
function safeauto_load_javascript_files() {

  wp_register_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '1.0', true );
  wp_register_script( 'respond', get_template_directory_uri() . '/js/respond.min.js', array('jquery'), '1.0', true );
  wp_register_script( 'breadcrumbs', get_template_directory_uri() . '/js/breadcrumbs.js', array('jquery'), '1.0', true );
  wp_register_script( 'fitvids', get_template_directory_uri() . '/js/jquery.fitvids.js', array('jquery'), '1.0', true );
  
 
  wp_enqueue_script( 'bootstrap' );
  wp_enqueue_script( 'respond' );
  wp_enqueue_script( 'breadcrumbs' );
  wp_enqueue_script( 'fitvids' );

 
}
function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');
?>