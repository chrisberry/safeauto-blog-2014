<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="featured-image-cont">
	
	<div class="full-width featured-image">
		<?php the_post_thumbnail() ?>
	</div>

</div>

<article class="post-content">
	<h1><?php the_title(); ?></h1>
	<h3 class="date"><?php the_time('l, F jS, Y'); ?></h3>	
	<h5 class="author"><span>By</span> <?php the_author_posts_link(); ?></h5>
	
	<div id="breadcrumbs" class="nav full-width">
		<?php if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('<div>','</div>');
	} ?>
		
	</div>
	
	<?php the_content(); ?>	
	<?php comments_template(); ?>	
	
	<?php endwhile; else: ?>
			<p><?php _e('Sorry, this page does not exist.'); ?></p>
	<?php endif; ?>
	 
	<div class="clearfix"></div>
</article>

	
<?php get_footer(); ?>