<?php get_header(); ?>
<!-- Content -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<article class="post">
		<?php if(has_post_thumbnail()): ?>
			<a class="thumbnail-wrapper" href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail() ?>
			</a>
		<?php endif ?>
		<div class="details">
			<div class="lead">
				<h2 class="headline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<span class="author"><?php the_author(); ?></span>
			</div>
			<?php the_excerpt() ?>
		    <?php comments_popup_link('No Comments', '1 Comment', '% Comments','post-buttons'); ?>
			<a href="<?php the_permalink(); ?>" class="post-buttons"><?php the_time('F j, Y'); ?></a>
			<a href="" class="post-buttons">Car Insurance</a>
			<div class="clearfix"></div>
		</div>
	</article>

<?php endwhile; else: ?>
		<p><?php _e('Sorry, there are no posts.'); ?></p>
<?php endif; ?>
	<div class="pager">
		<span class="previous"><?php previous_posts_link('Newer Entries') ?></span>
		<span class="next"><?php next_posts_link('Older Entries') ?></span>
		<div class="clearfix"></div>
	</div>
	
<div class="clearfix"></div>	
<?php get_footer(); ?>