<?php get_header(); ?>
<div class="span12">
	<div id="breadcrumbs" class="nav full-width">
		<?php if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('<div>','</div>');
	} ?>
		
	</div>
</div>
</div>
<div class="row main-content"><!-- left column --> 
  <div class="span8 flex-vids">
	 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	 <?php the_content(); ?>	
<?php endwhile; else: ?>
		<p><?php _e('Sorry, there are no posts.'); ?></p>
<?php endif; ?>
	<div class="pager">
		<span class="previous"><?php previous_posts_link('Newer Entries') ?></span>
		<span class="next"><?php next_posts_link('Older Entries') ?></span>
		<div class="clearfix"></div>
	</div>
  </div>
  <div class="span3 offset1">
	  <?php get_sidebar(); ?>	
  </div>
</div>
</div>
</div>
	
<?php get_footer(); ?>