<?php /* Template Name: Play It Safe Rewards */ ?>

<?php get_header(); ?>
<div class="span12">
	<div id="breadcrumbs" class="nav full-width">
		<?php if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('<div>','</div>');
	} ?>
		
	</div>
</div>
</div>
<div class="row main-content"><!-- left column --> 
  <div class="span8 flex-vids author">
	 <?php 
		$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
	 ?>
	 
	 <div class="author-info">
		 <h1><?php echo $curauth->nickname; ?></h1>
		 <span class="author-image">
		 	<?php echo get_avatar( get_the_author_meta( 'ID' ), 150 ); ?>
		 </span>
		 <p class="author-bio"><?php echo $curauth->description; ?></p>
		 <div class="clearfix"></div>
	 </div>
	 <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
	 <?php query_posts("author= $curauth->ID;&paged=$paged"); if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	 
	<article class="post">
						<div class="post-header">
							<h2>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h2>
						</div>
						<?php if(has_post_thumbnail()): ?>
						    <div class="post-image">
							<a href="<?php the_permalink(); ?>">
								<?php the_post_thumbnail() ?>
							</a>
						</div>
						<?php endif ?>
						<div class="post-summary">
							<p>
							<?php the_excerpt() ?>
							</p>
							<aside class="post-details">
								<span class="date">
										<a href="<?php the_permalink(); ?>"><?php the_time('l, F jS, Y'); ?></a> <i class="icon-calendar"></i>
								</span>
								<span class="comments">
										<?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?> <i class="icon-comment"></i>
								</span>
								<a href="<?php the_permalink(); ?>" class="btn btn-primary">Read More <i class="icon-chevron-right icon-white"></i></a>
							</aside>
						</div>
		<div class="clearfix"></div>				
	</article>

<?php endwhile; else: ?>
		<p><?php _e('Sorry, there are no posts.'); ?></p>
<?php endif; ?>
	<div class="pager">
		<span class="previous"><?php previous_posts_link('Newer Entries') ?></span>
		<span class="next"><?php next_posts_link('Older Entries') ?></span>
		<div class="clearfix"></div>
	</div>
  </div>
  <div class="span3 offset1">
	  <?php get_sidebar(); ?>	
  </div>
</div>
</div>
</div>
	
<?php get_footer(); ?>