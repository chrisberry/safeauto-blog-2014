<!-- left column -->
<!-- Quote Box
<div class="quote-box">
<h2>Get a Quote <i class="quote-icon"></i></h2>
<div class="quote-input"><form><input class="input-small" type="text" name="ZipCode" placeholder="Zipcode" />
<button class="btn btn-primary">Go <i class="icon-chevron-right icon-white"></i></button></form></div>
</div>
-->
<!-- secondary nav blog -->
<div class="side-box">
<h2><a href="http://blog.safeauto.com">
Blog <i class="blog-icon"></i>
</a></h2>
<nav class="nav">
<ul>
	<?php wp_list_pages(array('title_li' => '')); ?>
</ul>
</nav></div>
<!-- /secondary nav blog -->
<!-- secondary nav global -->
<div class="side-box">
<h2><a href="http://www.safeauto.com/about">
About Us <i class="trio-icon"></i>
</a></h2>
<nav class="nav">
<ul>
	<li><a href="http://www.safeauto.com/about/contact-us">Contact Us</a></li>
	<li><a href="http://www.safeauto.com/about/bilingual">Bilingual Careers</a></li>
    <li><a href="http://www.safeauto.com/about/FAQS">FAQ</a></li>
	<li><a href="http://blog.safeauto.com">Blog</a></li>
	<li><a href="http://www.safeauto.com/about/customer-service">Customer Service</a></li>
	<li><a href="http://www.safeauto.com/about/commercials">Commercials</a></li>
	<li><a href="http://www.safeauto.com/about/career-opportunities">Careers</a></li>
	<li><a href="http://www.safeauto.com/about/vintage-commercials">Vintage Commercials</a></li>
</ul>
</nav></div>
<!-- /secondary nav global -->

<!-- /left column -->