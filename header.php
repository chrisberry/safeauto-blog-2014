<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title><?php wp_title(); ?> | Safe Auto Insurance Company</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0"/>
    
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory'); ?>/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory'); ?>/css/style.css" rel="stylesheet">
    <?php wp_head(); ?>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js" type="text/javascript"></script>
    <![endif]-->  
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_bloginfo('template_directory');?>/img/Icon-72@2x.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_bloginfo('template_directory');?>/img/Icon@2x.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_bloginfo('template_directory');?>/img/Icon-72.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_bloginfo('template_directory');?>/img/Icon.png">
    <link rel="shortcut icon" href="<?php echo get_bloginfo('template_directory');?>/img/Icon.png">

  </head>

  
  	<body class="closed">

	    <div class="container">
	    	
	    		<div class="redline"></div>
	    		<a href="" class="logo">
	    			<img src="<?php echo get_bloginfo('template_directory');?>/img/logo.png" alt="<?php bloginfo('name'); ?>">
	    		</a>
	    		
	    		<header>
	    		
	    			<div class="header-content">
	    				<i class="fa fa-search"></i>
		    			<input type="search" placeholder="Search" class="search"/>
		    			<a class="link" href="#">SafeAuto.com</a>
		    			<a class="link" href="#">About SafeAuto</a>
		    			<a class="drawer-btn" data-toggle="drawer" data-target=".main-nav">
			    			<span class="bar"></span>
			    			<span class="bar"></span>
			    			<span class="bar"></span>
		    			</a>
		    		</div>
	    	
				</header>
	    	
	    	<!-- Navigation -->
	    	
	    		<nav class="main-nav drawer">
	    			<ul>
	    				<?php wp_list_pages(array('title_li' => '')); ?>
	    			</ul>
	    		</nav>
    	
			<!-- END Navigation -->
			
			<section class="content">