    </section>
    <footer>
				
				<div class="trio-wrapper">
					<div class="trio first">
						<h3>
							Who is SafeAuto?
						</h3>
						<p>
							Safe Auto Insurance Company is a property and casualty auto insurance carrier based in Columbus, Ohio. Founded in 1993, we are built on a philosophy of customer service, “The Diamond Principle.” This means we strive to provide caring, professional, knowledgeable, responsible and dedicated customer service to help make insurance affordable to every driver, no matter what their situation.
						</p>
					</div>
					
					<div class="trio">
						<h3>
							Who is SafeAuto?
						</h3>
						<p>
							Safe Auto Insurance Company is a property and casualty auto insurance carrier based in Columbus, Ohio. Founded in 1993, we are built on a philosophy of customer service, “The Diamond Principle.” This means we strive to provide caring, professional, knowledgeable, responsible and dedicated customer service to help make insurance affordable to every driver, no matter what their situation.
						</p>
					</div>
					
					<div class="trio last">
						<h3>
							Who is SafeAuto?
						</h3>
						<p>
							Safe Auto Insurance Company is a property and casualty auto insurance carrier based in Columbus, Ohio. Founded in 1993, we are built on a philosophy of customer service, “The Diamond Principle.” This means we strive to provide caring, professional, knowledgeable, responsible and dedicated customer service to help make insurance affordable to every driver, no matter what their situation.
						</p>
					</div>
					
					<div class="clearfix"></div>
				</div>
				
			</footer>

    </div><!-- /.container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <?php wp_footer(); ?>
    <?php wp_enqueue_script("jquery"); ?>
	<script>
	  jQuery(document).ready(function ($) {
	    // Target your .container, .wrapper, .post, etc.
	    $(".flex-vids").fitVids();
	  });
	</script>
<!-- AddThis Smart Layers BEGIN -->
<!-- Go to http://www.addthis.com/get/smart-layers to customize -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51e59faa578fb512"></script>
<script type="text/javascript">
  addthis.layers({
    'theme' : 'dark',
    'share' : {
      'position' : 'left',
      'numPreferredServices' : 5
    }, 
    'follow' : {
      'services' : [
        {'service': 'facebook', 'id': 'SafeAutoInsuranceCompany'},
        {'service': 'twitter', 'id': 'safeauto'},
        {'service': 'google_follow', 'id': '+safeauto'},
        {'service': 'youtube', 'id': 'legalforless'},
        {'service': 'instagram', 'id': 'safeauto'},
        {'service': 'rss', 'id': 'http://http://blog.safeauto.com/feed/'}
      ]
    },  
    'whatsnext' : {}  
  });
</script>
    <script src="<?php echo get_bloginfo('template_directory');?>/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    jQuery(document).ready(function ($) {
		var drawer = $("[data-toggle='drawer']").attr("data-target");
    	var mainContent = ".content";
		if ($(window).width() < 768) {
				$('body').addClass("closed");
			}
			else {
				$('body').removeClass("closed");
		};
		
		$(window).resize(function() {
		    	if ($(window).width() < 768) {
				  	$('body').addClass("closed");
				}
				else {
				   	$('body').removeClass("closed");
				}
		});
    	
    	$("[data-toggle='drawer']").click(function() {
			$('body').toggleClass("closed");
		});
	 });
	</script>
<!-- AddThis Smart Layers END -->


<div id="directions_extension" style="display: none;"></div></body></html>